info "Fixing wp-config... "
chmod 777 /opt/bitnami/wordpress/wp-config.php
php /fixconfig.php
mv /opt/bitnami/wordpress/wp-config.php /opt/bitnami/wordpress/wp-config.orig.php
mv /wp-config.php /opt/bitnami/wordpress/wp-config.php
info "Setting up plugins... "
mkdir /opt/bitnami/wordpress/logs
curl -fsSL 'https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar' -o /usr/local/bin/wp
chmod 777 /usr/local/bin/wp
wp --allow-root plugin update --all --path=/opt/bitnami/wordpress/ >> "/opt/bitnami/wordpress/logs/update-plugins.log"
while read plugin; do wp --allow-root plugin uninstall $plugin --path=/opt/bitnami/wordpress/ >> "/opt/bitnami/wordpress/logs/uninstall-plugins.log"; done < /uninstall-plugins
while read plugin; do wp --allow-root plugin install $plugin --activate --path=/opt/bitnami/wordpress/ >> "/opt/bitnami/wordpress/logs/install-plugins.log"; done < /install-plugins
wp --allow-root w3-total-cache import /w3-total-cache.json --path=/opt/bitnami/wordpress/
info "Fixing Permissions... "
mv /opt/bitnami/wordpress/wp-config.orig.php /opt/bitnami/wordpress/wp-config.php
sed -i 2i"define('WP_MEMORY_LIMIT', '512M');" /opt/bitnami/wordpress/wp-config.php
chown -R www-data:www-data /opt/bitnami/wordpress/wp-content
chown -R www-data:www-data /bitnami/wordpress/wp-content
chmod -R 777 /bitnami/wordpress/wp-content
chmod -R 777 /opt/bitnami/wordpress/wp-content
rm -rf /opt/bitnami/wordpress/wp-content/cache/tmp
chmod 777 /opt/bitnami/wordpress/.htaccess
rm -rf /opt/bitnami/wordpress/wp-content/cache/object
info "Setting up an empty git repo... "
apt-get -y install git
git init /opt/bitnami/wordpress/